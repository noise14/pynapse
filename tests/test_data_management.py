# -*- coding: utf-8 -*-

import os
import numpy as np
import pynapse as mm

DATA_DIR = os.path.dirname(mm.__file__) + '/../tests/data'


def test_seek_data():
    dataset = mm.seek_data(DATA_DIR)
    assert(len(dataset) > 0)


def test_load_data():
    dataset = mm.seek_data(DATA_DIR)
    for d in dataset:
        inputs, labels = mm.load_data(d)
        assert len(inputs) > 1
        assert inputs.shape[1] == len(labels)


def test_describe_data():
    dataset = mm.seek_data(DATA_DIR)
    inputs, labels = mm.load_data(dataset[0])
    dataset_desc = mm.describe_data(dataset[0])
    assert dataset_desc['dir'] + '/' + dataset_desc['name'] == dataset[0]
    assert dataset_desc['features'] == inputs.shape[0]


def test_normalization_train_test():
    dataset = mm.seek_data(DATA_DIR)
    inputs, labels = mm.load_data(dataset[0])
    xi, yi, xt, yt = mm.sample_train_test(inputs, labels, train_fraction=0.9,
                                          normalization=None, stratified=False)
    xi, xt = mm.normalize_train_test(xi, xt, normalization='zscore',
                                     cutoff=0.02, nan_to_num=True)
    assert np.mean(xi) < 1e-8
    assert abs(np.mean(np.std(xi, axis=1))-1) < 10*0.02


def test_sample_train_test():
    dataset = mm.seek_data(DATA_DIR)
    inputs, labels = mm.load_data(dataset[0])
    xi, yi, xt, yt = mm.sample_train_test(inputs, labels, train_fraction=0.9,
                                          normalization='zca', stratified=False)
    assert xi.shape[1] + xt.shape[1] == inputs.shape[1]

    xi, yi, xt, yt = mm.sample_train_test(inputs, labels, train_fraction=0.9,
                                          normalization='zscore')
    assert xi.shape[1] + xt.shape[1] == inputs.shape[1]
    # zscored : they should be close to zero
    assert (np.nanmean(xi, axis=1) < 0.01).all()


def test_binarize_labels():
    dataset = mm.seek_data(DATA_DIR)
    inputs, labels = mm.load_data(dataset[0])
    bin_labels = mm.binarize_labels(labels)

    unique_labels = np.unique(labels)
    for i,lab in enumerate(unique_labels):
        assert (bin_labels[np.nonzero(labels == lab)[0]] == -np.power(-1,i<0.5*len(unique_labels))).all()

def test_one_hot():
    labs = [0, 1, 2, 1, 1, 3]
    OH = mm.one_hot(labs)
    for i,l in enumerate(labs):
        assert(OH[l,i]==1)

def test_load_mnist():
    xi,yi,xt,yt = mm.load_mnist()
    assert(xi.shape == (784, 60000))
    assert(yi.shape == (10, 60000))
    assert(xt.shape == (784, 10000))
    assert(yt.shape == (10, 10000))

