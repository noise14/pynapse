# -*- coding: utf-8 -*-

from __future__ import division
from numpy import dot, sqrt, size, zeros, zeros_like, where, sign, unique
from numpy.random import rand
import warnings

from base_classifier import BaseClassifier


class Perceptron(BaseClassifier):

    """
    Rosenblatt's perceptron.

    Parameters
    ----------
    lrate : float, optional
        learning rate
    threshold : float, optional
        output threshold
    delta : float, optional
        margin

    Attributes
    ----------
    lrate : float
        learning rate
    threshold : float
        output threshold
    delta : float
        margin
    w : array
        output weights

    Example
    -------
    import os
    from numpy import mean, nonzero
    import missmatch as pn

    DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'

    datasets = pn.seek_data(DATA_DIR)
    inputs, labels = pn.load_data(datasets[0])

    xi, yi, xt, yt = pn.sample_train_test(inputs, pn.binarize_labels(labels))

    p = pn.Perceptron(lrate=0.05, iter=100)
    p.train(xi, yi)
    perf = p.test(xt, yt)
    print "Performance is", perf * 100
    """
    def __init__(self, iter=50, lrate=0.01, threshold=0, delta=0):
        """
        p = missmatch.Perceptron(lrate=0.02)
        """
        super(Perceptron, self).__init__(iter)

        self.lrate = lrate
        self.thr = threshold
        self.delta = delta

    def _update(self, x, y):
        h = dot(self.w, x)
        dh = y * h

        # Never compute sqrt(dot(self.w, self.w)) if delta == 0
        if self.delta > 0:
            delta = self.delta * sqrt(dot(self.w, self.w))
        else:
            delta = 0

        if dh <= delta:
            self.w += self.lrate * y * x

    def _train_setup(self, x, y):
        if unique(y).tolist() != [-1, 1]:
            warnings.warn("You're training with labels that are not -1 and 1.\
Is this really what you want to do?")
        if self.w is None:
            self.w = zeros(x.shape[0])

    def _predict(self, x):
        return sign(dot(self.w, x))


class SoftBounds(BaseClassifier):

    """
    A perceptron with soft bounds, i.e., synapses that exponentially approach
    hard bounds.

    Parameters
    ----------
    lrate : float, optional
        learning rate
    threshold : float, optional
        output threshold
    delta : float, optional
        margin

    Attributes
    ----------
    lrate : float
        learning rate
    threshold : float
        output threshold
    delta : float
        margin
    w : array
        output weights

    Example
    -------
    import os
    import missmatch as pn

    DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'

    datasets = pn.seek_data(DATA_DIR)
    inputs, labels = pn.load_data(datasets[0])

    xi, yi, xt, yt = pn.sample_train_test(inputs, labels)

    p = pn.SoftBounds(lrate=0.05)
    p.train(xi, yi)
    perf = p.test(xt, yt)
    print "Performance is", perf * 100
    """

    def __init__(self, iter=50, lrate=0.01, threshold=0, delta=0, g_i=0.5):

        super(SoftBounds, self).__init__(iter)

        self.lrate = lrate
        self.thr = threshold
        self.delta = delta
        self.g_i = g_i

    def _update(self, x, y):
        h = dot(self.w - self.g_i, x)
        dh = y * h

        # Never compute sqrt(dot(self.w, self.w)) if delta == 0
        if self.delta > 0:
            delta = self.delta * sqrt(dot(self.w, self.w))
        else:
            delta = 0

        if dh <= delta:
            delta_w = y * x
            self.w += self.lrate * ((delta_w > 0) * (1 - self.w) -
                                    (delta_w < 0) * self.w)

    def _predict(self, x):
        return sign(dot(self.w - self.g_i, x))


class BinaryStoch(BaseClassifier):

    """
    A perceptron with binary 0/1, stochastic synapses.

    Parameters
    ----------
    threshold : float, optional
        output threshold
    delta : float, optional
        margin
    pltp, pltd : float, optional
        probabilities of LTP and LTD
    g_i : float, optional
        global inhibition term
    auto_scale : bool, optional
        automatically scales inputs between zero and one:
        xp = self.Ga*x + self.Gb with 0<=xp<=1

    Attributes
    ----------
    threshold : float
        output threshold
    delta : float
        margin
    pltp, pltd : float
        probabilities of LTP and LTD
    g_i : float
        global inhibition term
    Ga, Gb : float
        global scaling and offset of input to bring components
        between zero and one
    w : array
        output weights

    Example
    -------
    import os
    import missmatch as pn

    DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'

    datasets = pn.seek_data(DATA_DIR)
    inputs, labels = pn.load_data(datasets[0])

    xi, yi, xt, yt = pn.sample_train_test(inputs, labels)

    p = pn.BinaryStoch(pltp=0.05, pltd=0.05)
    p.train(xi, yi)
    perf = p.test(xt, yt)
    print "Performance is", perf * 100
    """

    def __init__(self, iter=50, threshold=0, delta=0,
                 pltp=0.01, pltd=0.01, g_i=0.5, auto_scale=True):

        super(BinaryStoch, self).__init__(iter)

        self.thr = threshold
        self.delta = delta
        self.pltp = pltp
        self.pltd = pltd
        self.g_i = g_i

        if auto_scale:
            self.Ga = None
            self.Gb = None
        else:
            self.Ga = 1.0
            self.Gb = 0.0

    def G(self, x):
        """
        Scaling function
        """
        return self.Ga*x + self.Gb

    def _update(self, x, y):
        h = dot(self.w - self.g_i, self.G(x))

        # Never compute sqrt(dot(self.w, self.w)) if delta == 0
        if self.delta > 0:
            # since w is binary norm(w)=sum(w)
            delta = self.delta * sqrt(sum(self.w))
        else:
            delta = 0

        # LTD
        if y < 0 and h >= -delta:
            to_switch = rand(size(self.w)) < self.pltd
            self.w[to_switch] -= (self.G(x[to_switch]) > 0)\
                * self.w[to_switch]
        # LTP
        elif y > 0 and h <= delta:
            to_switch = rand(size(self.w)) < self.pltp
            self.w[to_switch] += (self.G(x[to_switch]) > 0)\
                * (1 - self.w[to_switch])

    def _predict(self, x):
        return sign(dot(self.w - self.g_i, self.G(x)))

    def _train_setup(self, x, y):
        # Auto-scale
        if self.Ga is None:
            min_val = x.min()
            self.Ga = 1.0/(x.max()-min_val)
            self.Gb = -self.Ga*min_val
        else:
            if x.min() < 0:
                warnings.warn("You're training with negative inputs. "
                              "Is this really what you want to do? "
                              "This algorithm assumes zero or positive "
                              "inputs.")
        super(BinaryStoch, self)._train_setup(self.G(x), y)


class BinaryStochMismatch(BaseClassifier):

    """
    A perceptron with binary 0/1, stochastic synapses and parameter variations.

    Parameters
    ----------
    threshold : float, optional
        output threshold
    delta : float, optional
        margin
    pltp, pltd : float, optional
        probabilities of LTP and LTD
    g_i : float, optional
        global inhibition term
    only_positive : bool
        whether to clip all negative weights to 0
    w_hi_mismatch : float
        variance of the gaussian distribution of weights values
    auto_scale : bool, optional
        automatically scales inputs between zero and one:
        xp = self.Ga*x + self.Gb with 0<=xp<=1

    Attributes
    ----------
    threshold : float
        output threshold
    delta : float
        margin
    pltp, pltd : float
        probabilities of LTP and LTD
    g_i : float
        global inhibition term
    w : array
        output weights (w = (1-w_s)*w_low + w_s*w_high)
    w_s : array
        synaptic states (0/1)
    w_hi : array
        synaptic weights of active synapses
    w_low : array
        synaptic weights of inactive synapses

    Example
    -------
    import os
    import missmatch as pn

    DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'

    datasets = pn.seek_data(DATA_DIR)
    inputs, labels = pn.load_data(datasets[0])

    xi, yi, xt, yt = pn.sample_train_test(inputs, labels)

    p = pn.BinaryStochMismatch(pltp=0.05, pltd=0.05, w_hi_mismatch=0.5)
    p.train(xi, yi)
    perf = p.test(xt, yt)
    print "Performance is", perf * 100

    See also
    --------
    BinaryStoch

    """
    def __init__(self, iter=50, threshold=0, delta=0,
                 pltp=0.01, pltd=0.01,
                 w_hi_mismatch=0, only_positive=False,
                 g_i=0.5, auto_scale=True):

        super(BinaryStochMismatch, self).__init__(iter)

        self.thr = threshold
        self.delta = delta
        self.pltp = pltp
        self.pltd = pltd
        self.g_i = g_i
        self.w_hi_mismatch = w_hi_mismatch
        self.only_positive = only_positive

        if auto_scale:
            self.Ga = None
            self.Gb = None
        else:
            self.Ga = 1.0
            self.Gb = 0.0

    def G(self, x):
        """
        Scaling function
        """
        return self.Ga*x + self.Gb

    def _update(self, x, y):
        h = dot(self.w - self.g_i, self.G(x))

        # Never compute sqrt(dot(self.w, self.w)) if delta == 0
        if self.delta > 0:
            delta = self.delta * sqrt(dot(self.w, self.w))
        else:
            delta = 0

        # LTD
        if y < 0 and h >= -delta:
            to_switch = rand(size(self.w)) < self.pltd
            self.w_s[to_switch] -= (self.G(x[to_switch]) > 0)\
                * self.w_s[to_switch]
        elif y > 0 and h <= delta:
            to_switch = rand(size(self.w)) < self.pltp
            self.w_s[to_switch] += (self.G(x[to_switch]) > 0)\
                * (1 - self.w_s[to_switch])
        self.w = self._states_to_weights()

    def _predict(self, x):
        return sign(dot(self.w - self.g_i, self.G(x)))

    def _states_to_weights(self):
        return where(self.w_s, self.w_hi, self.w_low)

    def _train_setup(self, x, y):
        # Auto-scale
        if self.Ga is None:
            min_val = x.min()
            self.Ga = 1.0/(x.max()-min_val)
            self.Gb = -self.Ga*min_val
        else:
            if x.min() < 0:
                warnings.warn("You're training with negative inputs. "
                              "Is this really what you want to do? "
                              "This algorithm assumes zero or positive "
                              "inputs.")

        if self.w is None:
            self.w_s = zeros(x.shape[0])
            self.w_hi = 1 + self.w_hi_mismatch * (2*rand(size(self.w_s)) - 1)
            if self.only_positive:
                self.w_low = zeros_like(self.w_s)
            else:
                self.w_low = self.w_hi_mismatch * (2*rand(size(self.w_s)) - 1)
            self.w = self._states_to_weights()
