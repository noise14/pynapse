The scripts in this folder can be run to automatically download common datasets into the data folder.
Datasets are downloaded in csv format.
Additionally, for datasets downloaded from the UCI repository, a text file is downloaded with brief descriptions on the type of data as retrieved from the database server.
The `describe.sh` script will create text files with .descr extension containing useful information on each dataset.

The comparisons_pipeline folder contains tools for comparing the classification performance of the developed models against a Support Vector Machine (SVM) using the libsvm library.
