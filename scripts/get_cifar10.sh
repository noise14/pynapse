#!/bin/bash

declare -a file=("cifar-10-python.tar.gz")
DEST="../data/"
REMOTE="http://www.cs.toronto.edu/~kriz/"

echo
echo " Dowloading CIFAR-10 data from "$REMOTE
echo

for i in "${file[@]}"
do
    if [ ! -f $DEST$i ]
    then
        wget -P $DEST $REMOTE$i
    fi

    cd $DEST
    tar zxvf $i
    cd -
done
