# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
import copy

from base_classifier import BaseClassifier


class Ensemble(BaseClassifier):

    """
    The Ensemble class aggregates multiple copies of a classifier into a single
    object.

    Attributes
    ----------
    base : BaseClassifier
    aggregation : string, optional
        aggregation procedure
        possible values: bagging, balanced_bagging, permutation

    Example
    -------
    import os
    import pynapse as pn

    DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'

    datasets = pn.seek_data(DATA_DIR)
    inputs, labels = pn.load_data(datasets[0])
    xi, yi, xt, yt = pn.sample_train_test(inputs, labels)

    per = pn.Perceptron(lrate=0.1, iter=50)
    ens = pn.Ensemble(per, n=10, aggregation='bagging')
    ens.train(xi, yi)
    perf_ens = ens.test(xt, yt)
    print "Performance of ensemble is ", perf * 100
    """

    def __init__(self, base, n=10, aggregation=None, flip_rate=0.2):
        self.base = base
        self.n = n
        self.aggregation = aggregation
        self.flip_rate = flip_rate
        self.models = []

        for n in xrange(self.n):
            model = copy.copy(self.base)
            self.models.append(model)

    def train(self, x, y):
        for model in self.models:

            if self.aggregation is 'bagging':
                ind = np.random.randint(0, x.shape[1], x.shape[1])
                model.train(x[:, ind], y[ind])

            elif self.aggregation is 'balanced_bagging':
                # Find class with less inputs. Sample that number of inputs for
                # all classes.
                labs = np.unique(y)     # unique labels
                ind_lab = []            # index for every label
                num_lab = np.zeros(len(labs))
                for i, lab in enumerate(labs):
                    ind_lab.append(np.nonzero(y == lab)[0])
                    num_lab[i] = len(ind_lab[-1])
                ind = []
                for ilab in ind_lab:
                    how_many = np.min(num_lab)
                    ind += np.random.permutation(ilab)[:].tolist()
                model.train(x[:, ind], y[ind])

            elif self.aggregation is 'permutation':
                ind = np.random.permutation(x.shape[1])
                model.train(x[:, ind], y[ind])

            elif self.aggregation is 'flip_output':
                ind = np.random.permutation(x.shape[1])
                flip = np.ones(x.shape[1])
                how_many = np.floor(self.flip_rate * x.shape[1])
                flip[np.random.permutation(x.shape[1])[:how_many]] = -1
                model.train(x[:, ind], y[ind]*flip)

            elif self.aggregation is None:
                model.train(x, y)

    def _predict(self, x):
        out = np.zeros((len(self.models), x.shape[1]))
        for i, model in enumerate(self.models):
            out[i, :] = model.predict(x)

        y = []
        for k in xrange(out.shape[1]):
            # find all unique elements and their positions
            uni, pos = np.unique(out[:, k], return_inverse=True)
            counts = np.bincount(pos)
            # count the number of each unique element
            counts += 1e-6*np.random.randn(len(counts))
            # find the positions of the maximum count
            y.append(uni[counts.argmax()])
        return y

    def test(self, x, y):
        min_size = np.min((x.shape[1], len(y)))
        return np.mean(self.predict(x[:, :min_size]) == y[:min_size])

    def pe(self, x, y):

        """
        ens.pe(x, y)

        Ths function returns the upper bound of the generalization error à la
        Breiman [1]:
            $$
            PE = \leq \bar \rho (1-s^2)s^2
            $$
        where $\bar \rho$ is the mean value of the correlation of the
        classifiers' responses and $s$ is the average 'strength' of the
        classifiers.

        [1] Breiman, Leo. "Random forests." Machine learning 45.1 (2001): 5-32.

        Parameters
        ----------
        x, y : arrays
            input patterns and labels

        Returns
        -------
        pe : float
            the generalization error
        """

        strength = self.test(x, y)
        pe = self.correlation(x) * (1 - strength ** 2) / strength ** 2
        return pe

    def correlation(self, x):

        """
        e.correlation(x)

        computes the average correlation between the responses of the ensemble
        units across inputs x

        Parameters
        ----------
        x : arrays
            input patterns

        Returns
        -------
        corr : float
            correlation of responses of output units
        """

        responses = np.zeros((len(self.models), x.shape[1]))
        for i, model in enumerate(self.models):
            responses[i, :] = model.predict(x)
        corr_sum = np.sum(np.corrcoef(responses))
        n = responses.shape[0]
        corr = (corr_sum - n) / ((n-1) * n)
        return corr

    def __len__(self):
        """x.__len__() <==> len(x)"""
        return len(self.models)

    def __getslice__(self, i, j):
        """x.__getslice__(i, j) <==> x[i:j]"""
        return self.models[i:j]

    def __getitem__(self, i):
        """x.__getitem__(i) <==> x[i]"""
        return self.models[i]

    def __call__(self, y):
        """x.__call__(y) <==> x(y) """
        return self.predict(y)
