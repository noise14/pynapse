# -*- coding: utf-8 -*-

from pynapse.base_classifier import BaseClassifier

import sys

sys.path.append('./libsvm-3.20/python/')

import svmutil

class Svm(BaseClassifier):

    """
    Implements a Support-Vector Machine using libsvm.
    """

    def __init__(self, params='-c 4'):

        super(Svm, self).__init__(None)

        self.params = params + ' -q' # adds 'quite' option
        self._svmutil = svmutil


    def train(self, x, y):

        prob = self._svmutil.svm_problem(y, x.T.tolist())
        param = self._svmutil.svm_parameter(self.params)
        self.m = self._svmutil.svm_train(prob, param)


    def test(self, x, y):

        labels, accuracy, vals =\
            self._svmutil.svm_predict(y, x.T.tolist(), self.m, '-q')
        return accuracy[0]/100.


    def predict(self, x):

        if len(x.shape) == 1:
            new_x = [x.tolist()]
            tmp_y = [1]
        else:
            new_x = x.T.tolist()
            tmp_y = [1] * len(new_x)
        labels, accuracy, vals =\
            self._svmutil.svm_predict(tmp_y, new_x, self.m, '-q')
        return labels if len(labels) > 1 else labels[0]
