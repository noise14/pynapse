#!/bin/bash

declare -a file=("train-images-idx3-ubyte.gz" "train-labels-idx1-ubyte.gz" "t10k-images-idx3-ubyte.gz" "t10k-labels-idx1-ubyte.gz")
DEST="../data/"
REMOTE="http://yann.lecun.com/exdb/mnist/"

echo
echo " Dowloading MNIST data from "$REMOTE
echo

for i in "${file[@]}"
do
    if [ ! -f $DEST$i ]
    then
        wget -P $DEST $REMOTE$i
    fi

    gzip -d $DEST$i
done
