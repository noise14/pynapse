# -*- coding: utf-8 -*-

from numpy import sign, random, dot, copy
import pynapse as pn

# p random patterns are generated and assigned to random labels
p = 20
X = sign(random.randn(2000, p))  # -1/1 inputs
Xbin = copy(X)  # 0/1 inputs
Xbin[Xbin<0] = 0
ws = sign(random.randn(X.shape[0]))
out = sign(dot(ws, X)-0.1)


def test_perceptron():
    p = pn.Perceptron(lrate=0.1, iter=100)
    p.train(X, out)
    perf = p.test(X, out)
    assert perf > 0.9


def test_softbounds():
    p = pn.SoftBounds(lrate=0.1, iter=100)
    p.train(X, out)
    perf = p.test(X, out)
    assert perf > 0.9


def test_binarystoch():
    p = pn.BinaryStoch(pltp=0.1, pltd=0.1, iter=100)
    p.train(Xbin, out)
    perf = p.test(Xbin, out)
    assert perf > 0.9


def test_binarystoch_mism():
    p = pn.BinaryStochMismatch(pltp=0.1, pltd=0.1, w_hi_mismatch=0.1, iter=100)
    p.train(Xbin, out)
    perf = p.test(Xbin, out)
    assert perf > 0.9
