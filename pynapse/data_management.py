# -*- coding: utf-8 -*-

from __future__ import division
import os
import csv
import numpy as np
import cPickle


DATA_DIR = '../data'

# File extensions that will be recognized as databases
EXT_DATABASE = ['csv']


def seek_data(directory=DATA_DIR):
    """
    dataset = seek_data(directory=DATA_DIR)

    Returns a list of the datasets in directory

    Parameters
    ----------
    directory : string, optional
        directory that needs to be inspected for datasets

    Returns
    -------
    dataset : tuple
        tuple that lists the file name of available datasets
    """
    dirlist = []
    try:
        dirlist = os.listdir(directory)
    except OSError:
        raise Exception("I cannot find directory " + directory)
    finally:
        dataset = []
        for filename in dirlist:
            if filename[-3:] in EXT_DATABASE:
                dataset.append(directory + '/' + filename)
        return tuple(dataset)


def load_data(filename):
    """
    inputs, labels = load_data(dataset[0])

    Parameters
    ----------
    filename : string
        file name of dataset

    Returns
    -------
    inputs, labels : numpy arrays (data is in column format)
    """
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        data, labels = [], []
        for row in reader:
            d = []
            for col in row[:-1]:
                d.append(float(col) if col != '' else np.nan)
            data.append(d)
            try:
                labels.append(float(row[-1]))
            except ValueError:
                labels.append(row[-1])
    labels = np.r_[labels]
    inputs = np.r_[data][:, :-1].T
    return inputs, labels


def describe_data(filename):
    """
    dataset_desc = describe_data(dataset[0])

    Parameters
    ----------
    filename : string
        file name of dataset

    Returns
    -------
    dataset_sec : dictionary
        description of dataset (name, size, classes, etc.)
    """
    inputs, labels = load_data(filename)

    dataset_desc = {'name': os.path.basename(filename),
                    'dir': os.path.dirname(filename),
                    'samples': inputs.shape[1],
                    'features': inputs.shape[0],
                    'classes': len(np.unique(labels)),
                    'miss_data': sum(np.isnan(inputs.flatten()))
                    }
    return dataset_desc


def normalize_train_test(xi, xt, normalization='zscore',
                         cutoff=0.05, nan_to_num=True):
    """
    xi_normalized, xt_normalized = sample_train_test(xi, xt, cutoff=0.1)

    Normalizes the training set xi and applies the same normalization to xt

    Parameters
    ----------
    xi : array
        input training patterns

    xt : array
        input testing patterns

    normalization : string, optional
        what type of normalization should be applied to the data
        (training set is used to determine normalization factors)
        possible values: 'zscore', 'zca'

    cutoff : double, optional
        cutoff to be applied to zscore normalization

    nan_to_num : bool, optional
        whether empty features have to be replaced with 0

    Returns
    -------
    xi_normalized : array
        normalized training set of patterns

    xt_normalized : array
        test set of patterns normalized in the same way as xi
    """
    if normalization == 'zscore':
        m = np.nanmean(xi, axis=1)
        s = np.nanstd(xi, axis=1) + cutoff
        xi_normalized = (xi - m[:, np.newaxis]) / (s[:, np.newaxis] + cutoff)
        xt_normalized = (xt - m[:, np.newaxis]) / (s[:, np.newaxis] + cutoff)

    elif normalization == 'zca':
        m = np.nanmean(xi, axis=1)
        S = np.cov(np.nan_to_num(xi))
        lam, vec = np.linalg.eigh(S)    # eigh is for symmetric matrices
        ZCA = np.dot(vec / (np.sqrt(lam) + cutoff), vec.T)
        xi_normalized = np.dot(ZCA, xi - m[:, np.newaxis])
        xt_normalized = np.dot(ZCA, xt - m[:, np.newaxis])

    else:
        raise Exception("Normalization type \"" + str(normalization) +
                        "\" not defined")
    if nan_to_num:
        return np.nan_to_num(xi_normalized), np.nan_to_num(xt_normalized)
    else:
        return xi_normalized, xt_normalized


def sample_train_test(inputs, labels, train_fraction=0.5, stratified=True,
                      normalization='zscore', cutoff=0.05, nan_to_num=True,
                      trans=None, **trans_args):
    """
    xi, yi, xt, yt = sample_train_test(inputs, labels, train_fraction=0.9,
                                       stratified=True, cutoff=0.1)

    Divides dataset in training and test set

    Parameters
    ----------
    inputs : array
        input patterns

    labels : array
        labels of patterns

    train_fraction : double, optional
        fraction of patterns assigned to training set

    stratified : bool, optional
        whether sampling is stratified within classes

    normalization : string, optional
        what type of normalization should be applied to the data
        (training set is used to determine normalization factors)
        possible values: 'zscore', 'zca', None

    cutoff : double, optional
        cutoff to be applied to zscore normalization

    nan_to_num : bool, optional
        whether empty features have to be replaced with 0

    trans : function, optional
        a transformation function to be applied to all vectors of xi and xt

    **trans_args : various types, optional
        all arguments to be passed to the trans function

    Returns
    -------
    xi : array
        training set of patterns

    yi : array
        labels of training set

    xt : array
        test set of patterns

    yt : array
        labels of test set
    """
    # Determine training and test set
    ind_train = []
    ind_test = []

    if stratified:
        labs = np.unique(labels)
        for l in labs:
            ind = np.nonzero((labels == l).flatten())[0]
            np.random.shuffle(ind)
            ind_train = ind_train + \
                ind[:round(train_fraction * len(ind))].tolist()
            ind_test = ind_test + \
                ind[round(train_fraction * len(ind)):].tolist()
    else:
        ind = np.linspace(0, len(labels) - 1, len(labels))
        np.random.shuffle(ind)
        ind_train = ind_train + ind[:round(train_fraction * len(ind))].tolist()
        ind_test = ind_test + ind[round(train_fraction * len(ind)):].tolist()

    # Normalize
    xi = inputs[:, ind_train]
    yi = labels[ind_train]
    xt = inputs[:, ind_test]
    yt = labels[ind_test]

    if normalization is not None:
        xi, xt = normalize_train_test(xi, xt, normalization=normalization,
                                      cutoff=cutoff, nan_to_num=nan_to_num)

    # Transform
    if trans is not None:
        xi = trans(xi, **trans_args)
        xt = trans(xt, **trans_args)

    return xi, yi, xt, yt


def binarize_labels(labels, frac_pos=0.5, pos_labels=None):
    """
    bin_labels = binarize_labels(labels, frac_pos=0.5)

    Binarizes labels by setting them to -1 or 1

    Parameters
    ----------
    labels : array
        labels of patterns

    frac_pos : double, optional
        fraction of labels that are assigned to +1;
        the rest is assigned to -1;
        this is ignored if pos_labels is specified

    pos_labels : list, optional
        list of labels that are assigned to +1;
        the rest is assigned to -1;
        this does not refer to the actual labels, but rather to their order;
        if this is specified, frac_pos is ignored

    Returns
    -------
    bin_labels : array
        labels binarized to -1 and 1
    """
    unique_labels = np.unique(labels)
    bin_labels = -np.ones(labels.size)

    if pos_labels is not None:
        for i, lab in enumerate(unique_labels):
            if i in pos_labels:
                bin_labels[np.nonzero(labels == lab)[0]] = 1
    else:
        pos_threshold = frac_pos*len(unique_labels)
        for i, lab in enumerate(unique_labels):
            if i < pos_threshold:
                bin_labels[np.nonzero(labels == lab)[0]] = 1
    return bin_labels


def one_hot(labels, n=None):
    """
    one_hot_lab = one_hot(labels, n=None)

    Transform label representation in vector one-hot representation. So for
    every label it gives a vector which is all zero, apart from component l.
    Note that enumeration starts from zero, not one.

    Parameters
    ----------
    labels : array
        labels of patterns

    n : integer, optional
        number of distinct labels; if not given it computes it from labels
    """
    if type(labels) == list:
        labels = np.array(labels)
    labels = labels.flatten()
    if n is None:
        n = len(np.unique(labels))
    one_h = np.zeros((n, len(labels)))
    one_h[labels, np.arange(len(labels))] = 1
    return one_h


def load_mnist(directory=DATA_DIR, ntrain=60000, ntest=10000, onehot=True):
    """
    x_train, y_train, x_test, y_test = load_mnist()

    Loads MNIST data obtained by the get_mnist.py script.

    Parameters
    ----------
    directory : string
        data folder

    ntrain : integer
        total number of training samples

    ntest : integer
        total number of test samples

    onehot : bool
        if True, labels are represented as 0/1 arrays where one corresponds to
        the given label

    Returns
    -------
    x_train, y_train, x_test, y_test : numpy arrays (data is in column format)
    """
    with open(os.path.join(directory, 'train-images-idx3-ubyte'), 'rb') as fd:
        loaded = np.fromfile(file=fd, dtype=np.uint8)
        xi = loaded[16:].reshape((60000, 28*28)).astype(float)

    with open(os.path.join(directory, 'train-labels-idx1-ubyte'), 'rb') as fd:
        loaded = np.fromfile(file=fd, dtype=np.uint8)
        yi = loaded[8:].reshape((60000))

    with open(os.path.join(directory, 't10k-images-idx3-ubyte'), 'rb') as fd:
        loaded = np.fromfile(file=fd, dtype=np.uint8)
        xt = loaded[16:].reshape((10000, 28*28)).astype(float)

    with open(os.path.join(directory, 't10k-labels-idx1-ubyte'), 'rb') as fd:
        loaded = np.fromfile(file=fd, dtype=np.uint8)
        yt = loaded[8:].reshape((10000))

    xi = xi/255.
    xt = xt/255.

    xi = xi[:ntrain].T
    yi = yi[:ntrain]

    xt = xt[:ntest].T
    yt = yt[:ntest]

    if onehot:
        yi = one_hot(yi, 10)
        yt = one_hot(yt, 10)
    else:
        yi = np.asarray(yi)
        yt = np.asarray(yt)

    return xi, yi, xt, yt


def load_cifar10(directory=DATA_DIR, batches=[1, 2, 3, 4, 5],
                 ntrain=10000, ntest=10000):
    """
    inputs, labels = load_cifar10()

    Loads cifar10 data obtained by the get_cifar10.py script.

    Parameters
    ----------
    directory : string
        data folder

    ntrain : integer
        total number of training samples

    ntest : integer
        total number of test samples

    batches : list
        which data batches to load (1 to 5 included)

    Returns
    -------
    inputs, labels : numpy arrays (data is in column format)
    """
    batch_data = []
    batch_labels = []
    for b in batches:
        with open(directory + '/cifar-10-batches-py/data_batch_%d' % b,
                  'rb') as f:
            dict = cPickle.load(f)
        batch_data.append(dict['data'][:ntrain].T)
        batch_labels.append(np.r_[dict['labels']][:ntrain].T)
    xi = np.column_stack(batch_data)
    yi = np.concatenate(batch_labels)
    with open(directory+'/cifar-10-batches-py/test_batch', 'rb') as f:
        dict = cPickle.load(f)
    xt = dict['data'][:ntrain].T
    yt = np.r_[dict['labels'][:ntrain]]
    return xi, yi, xt, yt
