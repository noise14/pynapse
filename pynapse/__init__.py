# -*- coding: utf-8 -*-
'''
pynapse

Toplevel functions
------------------
- seek_data: looks for datasets in the argument directory or in ./data
- load_data: loads a dataset
- load_mnist: loads MNIST dataset
- load_cifar10: loads CIFAR-10 dataset
- describe_data: loads a dataset and parses samples number, classes, etc.
- sample_train_test: divides a given dataset into a training and a test set

Modules
-------

Example
-------

::

    import pynapse as pn

    dataset = pn.seek_data()  # looks at what datasets are present in ./data
    inputs, labels = pn.load_data(dataset[0])  # load the first dataset

'''

try:
    from .data_management import (seek_data, load_data, describe_data,
                                  normalize_train_test, sample_train_test,
                                  binarize_labels, one_hot, load_mnist,
                                  load_cifar10)
    from .linear_classifiers import (Perceptron, SoftBounds, BinaryStoch,
                                     BinaryStochMismatch)
    from .multi import Multi
    from .ensemble import Ensemble
except ImportError as e:
    import sys
    print >>sys.stderr, '''\
Could not import submodules (exact error was: %s).

There are many reasons for this error the most common one is that you have
either not built the packages or have built (using `python setup.py build`) or
installed them (using `python setup.py install`) and then proceeded to test
pynapse **without changing the current directory**.

Try installing and then changing to another directory before importing
pynapse.
''' % e

__all__ = [
    'seek_data', 'load_data', 'describe_data', 'normalize_train_test',
    'sample_train_test', 'binarize_labels', 'load_mnist', 'one_hot',
    'Perceptron', 'SoftBounds', 'BinaryStoch', 'BinaryStochMismatch',
    'Multi', 'Ensemble'
]
