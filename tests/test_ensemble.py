# -*- coding: utf-8 -*-

import os
from numpy import *
import pynapse as pn

DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'
dataset = pn.seek_data(DATA_DIR)
inputs, labels = pn.load_data(dataset[0])
labels = pn.binarize_labels(labels)

xi, yi, xt, yt = pn.sample_train_test(inputs, labels, train_fraction=0.9,
                                      normalization='zscore', stratified=False)
# normalize data
xi, xt = pn.normalize_train_test(xi, xt, normalization='zscore',
                                 cutoff=0.001, nan_to_num=True)

per = pn.Perceptron(lrate=0.1, iter=10)

def test_ensemble_bagging():
    ens = pn.Ensemble(per, n=100, aggregation='bagging')
    ens.train(xi, yi)
    perf_train = ens.test(xi, yi)
    perf_test = ens.test(xt, yt)
    print ' Bagging: perf_test = ' + str(perf_test)
    assert perf_train > 0.90
    assert perf_test > 0.90

def test_ensemble_flip_output():
    ens = pn.Ensemble(per, n=100, aggregation='flip_output', flip_rate=0.25)
    ens.train(xi, yi)
    perf_train = ens.test(xi, yi)
    perf_test = ens.test(xt, yt)
    print ' Flip output: perf_test = ' + str(perf_test)
    assert perf_train > 0.90
    assert perf_test > 0.90

def test_ensemble_measures():
    ens = pn.Ensemble(per, n=100, aggregation='bagging')
    ens.train(xi, yi)
    perf_test = ens.test(xt, yt)
    assert 1-perf_test <= ens.pe(xt, yt)
