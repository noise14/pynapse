#!/usr/bin/env python
# coding: utf-8

import copy
import numpy as np
import missmatch as mm
from libsvm import Svm


class RCN():
    """
    An object that generates and handles RCNs. Weights are taken randomly from a
    normal distribution.
    A convenient use of this class is:
        projections = RCN(input_patterns)

    Attributes:
    ===========
    activity : array
        the activity across the RCNs
    activation_func : function
        the non-linearity function (e.g., tanh)
    bias : float
        a bias term

    Example:
    ========
    >>> import numpy as np

    >>> rcns = RCN(N=100, func=tanh, bias=0.1)
    >>> input = np.random.rand((10, 5)) # 5 patterns of size 10
    >>> output = rcns(input) # 5 patterns of size 100
    >>> print input.shape
    (10, 5)
    >>> print output.shape
    (100, 5)
    """

    def __init__(self, N, func, bias):
        self.activity = np.zeros(N)
        self.activation_func = func
        self.bias = bias

    def __call__(self, x):
        try:
            self.w
        except AttributeError:
            self._gen_weights(x.shape[0])
        self.activity = self.activation_func(np.dot(self.w, x) - self.bias)
        return self.activity

    def _gen_weights(self, dims):
        self.w = np.random.normal(size=(len(self.activity), dims))

# assumes you have missmatch-compatible data in ../../data
datasets = mm.seek_data('../../data')

# Load breast_cancer dataset
i_dat = [k for k in xrange(len(datasets)) if 'breast' in datasets[k]][0]

print "\n"
print " Loading %s" % datasets[i_dat]
inputs, labels = mm.load_data(datasets[i_dat])
labels = mm.binarize_labels(labels)


# Set parameters:
n_RCNs = 2*len(labels)
n_out = 25
Repetitions = 10

# List of classifiers to test
classifiers = [
    mm.Ensemble(mm.BinaryStoch(iter=10), n=n_out, aggregation=None), # Ensemble of binary stochastic perceptrons:
    mm.Perceptron(iter=200),
    Svm()
]

# List of data transformation for the input of each classifier
data_trans = [
    RCN(n_RCNs, np.tanh, 0),# project input into the hidden layer of RCNs
    lambda x: x,            # identity transformation
    lambda x: x
]


# Start pipeline:
if __name__ == "__main__":

    perf_train = np.zeros((len(classifiers),Repetitions))
    perf_test = np.zeros((len(classifiers),Repetitions))

    print " Will use %d RCNs" % n_RCNs
    print " Will use ensemble of %d classifiers" % n_out

    for rep in xrange(Repetitions):

        # separates data into train and test set
        xi, yi, xt, yt = mm.sample_train_test(inputs, labels, train_fraction=0.9, stratified=False,
                                              normalization='zscore', cutoff=0.001, nan_to_num=True)
        for i, (cl, tr) in enumerate(zip(classifiers,data_trans)):
            model = copy.copy(cl)
            model.train(tr(xi), yi)
            perf_train[i,rep] = model.test(tr(xi), yi)
            perf_test[i,rep] = model.test(tr(xt), yt)

    # Print results:
    for i in xrange(len(classifiers)):
        print " - Test performance of classifier %d :" % i, np.mean(perf_test[i,:])

    print "\n"
