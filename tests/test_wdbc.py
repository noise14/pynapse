# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# Wisconsin Diagnostic Breast Cancer
# ==================================
#
# Class distribution: 357 benign, 212 malignant
# Number of features: 30
# State of art: 99.6%

import os
import numpy as np
import pynapse as pn

DATA_DIR = os.path.dirname(pn.__file__) + '/../tests/data'


def test_breast_cancer_ensemble():
    # read data
    datasets = pn.seek_data(DATA_DIR)
    # could use python package `re`
    ind_dat = np.where(
        map(lambda x: 'breast-cancer' in x, map(os.path.basename, datasets)))[0]
    inputs, labels = pn.load_data(datasets[ind_dat])

    # parameters of learning
    n_unique_labels = len(np.unique(labels))
    n_dimensions = inputs.shape[0]
    n_patterns = inputs.shape[1]
    # number of randomly connected neurons
    n_RCNs = np.ceil(n_patterns / n_unique_labels) * 5
    # n_RCNs = 16000
    print "Number of RCNs", n_RCNs

    # w_rcn = 1 # use this to remove the RCNs (unsafe!)
    w_rcn = np.random.randn(
        n_RCNs,
        n_dimensions) / np.sqrt(
        n_dimensions)  # reaches 96%
    print "Shape of the RCN projection matrix", w_rcn.shape

    # preparing the dataset: xi, yi: training set; xt, yt: test set
    xi, yi, xt, yt = pn.sample_train_test(inputs, labels, train_fraction=0.9,
                                          stratified=True, cutoff=0.1,
                                          normalization='zscore')

    # applying RCN projection and non-linearity
    thr = 0.
    gain = 1.
    xi = np.tanh(gain * (np.dot(w_rcn, xi) - thr))
    xt = np.tanh(gain * (np.dot(w_rcn, xt) - thr))
    print "N train: %d, N test: %d" % (len(xi[0, :]), len(xt[0, :]))

    # learning
    base_unit = pn.Perceptron()
    multi = pn.Multi(base_unit, iter=10)
    multi.train(xi, yi)

    # Test performance
    assert multi.test(xt, yt) > 0.9
