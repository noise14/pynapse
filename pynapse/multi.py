# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
import copy

from base_classifier import BaseClassifier


class Multi(BaseClassifier):

    """
    Reduces multi-class classifications to a series of binary classifications.
    base must obey the classifier interface.

    multi_classifier = multi(binary_classifier, code)

    Parameters
    ----------
    binary_classifier : BaseClassifier

    code : array or string, optional
        type of output code to turn multi-class problem to sequence of binary
        ones (possible values: 'one-vs-rest', 'pairs', or a generic ECOC matrix)
    """

    def __init__(self, base, iter=50, code='one-vs-rest', balance='max'):
        self.base = base
        self.iter = iter
        self.code = code
        self.balance = balance
        self.labels = None
        self.models = None

    def train(self, x, y):
        if self.labels is None:
            self.labels = np.sort(np.unique(y))
        elif not all([i in self.labels for i in np.unique(y)]):
            raise Exception("There are new labels. "
                            "Dynamically adding new labels in multi is still "
                            "not supported")

        # Transform every case to a binary ECOC with -1/1 outputs
        n_labels = len(self.labels)
        self.ECOC = code_to_ECOC(self.code, n_labels)

        if self.models is None:
            self.models = []

        if self.balance == 'online':
            pass
        else:
            for n in xrange(self.ECOC.shape[0]):
                ind = []
                for si in xrange(2):  # itrate over positive and negative sign
                    which = np.nonzero(self.ECOC[n, :] == pow(-1, si))[0]
                    lab = self.labels[which]
                    ind.append([i[0] for i in enumerate(y) if i[1] in lab])

                # balancing
                if self.balance == 'max':
                    n_samp = max(len(ind[0]), len(ind[1]))

                elif self.balance == 'min':
                    n_samp = min(len(ind[0]), len(ind[1]))

                elif isinstance(self.balance, int):
                    n_samp = self.balance

                else:
                    raise Exception("Argument BALANCE has a non-supported type")

                # ind_pos and ind_neg are lists
                ind_all = int(n_samp/len(ind[0])) * ind[0] +\
                    ind[0][:np.mod(n_samp, len(ind[0]))]
                ind_all += int(n_samp/len(ind[1])) * ind[1] +\
                    ind[1][:np.mod(n_samp, len(ind[1]))]
                np.random.shuffle(ind_all)

                # iterate over ECOC outputs
                if len(self.models) != self.ECOC.shape[0]:
                    model = copy.copy(self.base)
                else:
                    model = self.models[n]
                y_ind = [np.argwhere(i == self.labels)[0][0]
                         for i in y[ind_all]]
                model.train(x[:, ind_all], self.ECOC[n, y_ind])
                if len(self.models) != self.ECOC.shape[0]:
                    self.models.append(model)

    def _predict(self, x):
        out = np.zeros((len(self.models), x.shape[1]))
        for i, model in enumerate(self.models):
            out[i, :] = model.predict(x)
        H = np.dot(self.ECOC.T, out)
        return self.labels[np.argmax(H, axis=0)]

    def test(self, x, y):
        min_size = np.min((x.shape[1], len(y)))
        return np.mean(self.predict(x[:, :min_size]) == y[:min_size])

    def confusion(self, x, y):

        """
        multi.confusion(x, y)

        returns the confusion matrix on the given dataset

        Parameters
        ----------
        x, y : arrays
            input patterns and labels

        Returns
        -------
        conf : array
            confusion matrix
        """

        outs = self.predict(x)
        conf = np.r_[[[sum((outs == l) * (y == m))
                       for l in self.labels]
                      for m in self.labels]]
        return conf

    def __repr__(self):
        return str(self.ECOC)


def code_to_ECOC(code, n_labels):
    """
    transform every case to a binary ECOC with -1/1 outputs
    """
    if code == 'one-vs-rest':
        ECOC = -np.ones((n_labels, n_labels)) + 2*np.eye(n_labels)

    elif code == 'pairs':
        ECOC = np.zeros((n_labels*(n_labels-1)/2, n_labels))
        t = 0
        for i in xrange(n_labels):
            for j in xrange(i+1, n_labels):
                ECOC[t, i] = 1
                ECOC[t, j] = -1
                t += 1

    elif isinstance(code, type(np.array([]))):
        if code.shape[1] == n_labels:
            ECOC = code
        else:
            raise Exception("Matrix code must to have one column per label")
    else:
        raise Exception("Argument CODE has a non-supported format")

    return ECOC
