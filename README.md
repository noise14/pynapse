# pynapse #

pynapse consists of a minimalistic set of Python tools to test ensemble methods on classification tasks using neural networks with realistic synapses as basic units.
The name pynapse comes from the combination of "Python" and "synapse" and is pronounced "pin-ups".

Many of the ideas behind this project originated during the [Cognitive Neuromorphic Engineering Workshop](https://capocaccia.ethz.ch) held yearly in Capo Caccia, Alghero (Italy).

This project is released under the terms of the Eclipse Public License (EPL).
The package contains a redistribution of the libsvm code.
Please refer to its copyright notice.


## Get started ##

Make sure you have:

* numpy

They can be installed with:

    $ sudo easy_install numpy

We suggest to configure numpy to use compiled libraries such as BLAS.
Please refer to numpy's documentation on how to achieve this.

To get started:

    $ git clone git@bitbucket.org:noise14/pynapse.git

Install:

    $ cd pynapse
    $ python setup.py install

Test:

    $ cd tests
    $ nosetests

Under the `scripts` folder you can find a set of useful scripts to download publicly available databases from the UCI repository and other sources:

    $ cd ../scripts
    $ sh get_uci.sh
    $ sh get_mnist.sh
    $ sh get_cifar-10.sh

Datasets will be downloaded into the `pynapse/data` folder.


## Contribution guidelines ##

Please keep the code [clean](http://legacy.python.org/dev/peps/pep-0008/).
The master branch is meant to be the stable version. Any other branch must be considered under development and thus unstable.
You are welcome to fork the project, make your own modifications and then make a pull request.
See the gitflow guidelines [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).


## Contacts ##

 - Fabio Stefanini
 - Mattia Rigotti

## Other resources ##

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
