# -*- coding: utf-8 -*-
try:
    from setuptools import setup, find_packages
except:
    print '''
setuptools not found.

On linux, the package is often called python-setuptools'''
    from sys import exit
    exit(1)


setup(name='pynapse',
      version='1.0beta',
      description='',
      author='',
      author_email='',
      url='',
      packages = find_packages(),
      package_data={'pynapse': ['../tests/data/*.csv']},
      test_suite = 'nose.collector',
      )
