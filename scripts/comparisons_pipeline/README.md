The script in this folder can be used to compare the classification performance of the develoepd models to a Support Vector Machine (SVM).
The script uses the libsvm library which is publicly available and is redistributed here for convenience.
In order to run the scripts you must proceed with the following steps (for Unix systems).
Extract the source code for libsvm:
    
    $> tar zxvf libsvm-3.20.tar.gz
    $> cd libsvm-3.20
    $> make
    $> cd python
    $> make

In libsvm-3.20/python you find the python wrappers used by our scripts.
Refer to the [libsvm website](http://www.csie.ntu.edu.tw/~cjlin/libsvm/) for further instructions.

In libsvm.py a classifier is implemented which is compatible with pynapse application framework.
Usage example:

    from libsvm import Svm
    import pynapse as pn
    x, y = pn.load_data(...)
    xi, yi, xt, yt = pn.sample_train_test(x, y)
    p = Svm()
    p.train(xi, yi, 1)
    p.test(xi, yi)  # hit rate on training data
    p.test(xt, yt)  # hit rate on test data
    p.predict(xi[:, :10])  # predicted labels of first 10 patterns

Follow the example.py script as a template for comparing classification performances against SVM.
To run the example script, don't forget to download the UCI datasets using the scripts in the scripts folder.
