# -*- coding: utf-8 -*-

from numpy import *
import pynapse as pn

# p random patterns are generated and assigned to random labels
p = 20
X = sign(random.randn(1000, p))
out = random.choice(['a', 'b', 'c'], X.shape[1])  # three classes

per = pn.Perceptron(lrate=0.1, iter=50)

def test_multi_onevsrest():
    my_multi = pn.Multi(per, iter=10, code='one-vs-rest')
    my_multi.train(X, out)
    perf = my_multi.test(X, out)
    assert perf > 0.9

def test_multi_pairs():
    my_multi = pn.Multi(per, iter=10, code='pairs')
    my_multi.train(X, out)
    perf = my_multi.test(X, out)
    assert perf > 0.9

def test_confusion():
    my_multi = pn.Multi(per, iter=10, code='pairs')
    my_multi.train(X, out)
    perf = my_multi.test(X, out)
    conf = my_multi.confusion(X, out)
    assert sum(conf) == len(out)
    assert sum(diag(conf)) == perf*len(out)
