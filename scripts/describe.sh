#!/bin/bash

# This simple bash scripts outputs the counts of each class in a dataset in a
# .descr file. In this way it

DATA_FOLDER='../data/'

for file in $DATA_FOLDER*csv; do
	outfile=`echo $file | sed -e 's/csv$/descr/g'`
	echo -n > $outfile
	echo "# total no. of patterns" >> $outfile
	wc -l < $file >> $outfile
	echo "# no. of features per pattern" >> $outfile
	awk -F"," 'END{print NF}' < $file >> $outfile
	echo "# class counts" >> $outfile
	awk -F"," '{print $NF}' < $file | sort | uniq -c | sort -nr >> $outfile
done
