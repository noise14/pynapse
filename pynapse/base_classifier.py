# -*- coding: utf-8 -*-

from numpy import zeros, dot, mean, sign, min


class BaseClassifier(object):
    """
    The base class of a linear classifier. This class shouldn't be explicitly
    used but instead child classes (e.g., pynapse.Perceptron) should be used.
    Notice that the `repr` of a BaseClassifier is the array of its weights.

    Attributes
    ----------
    w : array
        output weights
    """
    def __init__(self, iter):
        self. w = None
        self.iter = iter

    def train(self, x, y):
        """
        b.train(x, y)

        Train classifier with inputs x and labels y.
        If you want to train for several epochs call train several times.
        If you want to permute the presentation order,
        permute the arguments x and y.

        Parameters
        ----------
        x, y : arrays
            patterns and labels of the training set
        """
        self._train_setup(x, y)

        for n in xrange(self.iter):
            for i in xrange(x.shape[1]):
                self._update(x[:, i], y[i])
        return

    def predict(self, x):
        """
        output = b.predict(x)

        produces an output given an input pattern using the internal parameters

        Parameters
        ----------
        x : array
            input pattern

        Returns
        -------
        out : float
            the activation level
        """
        return self._predict(x)

    def test(self, x, y):
        """
        perf = b.test(x, y) * 100

        returns the hit rate on a given dataset

        Parameters
        ----------
        x, y : arrays
            input patterns and labels

        Returns
        -------
        rate : floate
            the hit rate
        """
        min_size = min((x.shape[1], len(y)))
        return mean(self.predict(x[:, :min_size]) == y[:min_size])

    def _predict(self, x):
        """
        This function returns the response of the classifier and is wrapped
        within the `__call__` method. The default implementation returns a dot
        product between the input and the weights.
        """
        return sign(dot(self.w, x))

    def _train_setup(self, x, y):
        """
        This function is used in the `train` method. It is called before
        `_train` and is offered to implement all the necessary operations to
        correctly operate the `train` function. The default implementation
        does nothing.
        """
        if self.w is None:
            self.w = zeros(x.shape[0])

    def __call__(self, x):
        """x.__call__(...) <==> x(...)"""
        return self.predict(x)

    def __getitem__(self, i):
        """x.__getitem__(i) <==> x[i]"""
        return self.w[i]

    def __getslice__(self, i, j):
        """x.__getslice__(i, j) <==> x[i:j]"""
        return self.w[i:j]

    def __len__(self):
        """x.__len__() <==> len(x)"""
        return len(self.w)

    def __repr__(self):
        return str(self.w)
