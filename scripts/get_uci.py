#!/usr/bin/python
# Download some datasets from the UCI repository and format them in CSV format.
# It also eliminates non informative columns, positionis the class label as the
# last column, and insert an empty space (,,) for missing values

import urllib
import numpy
import os
import gzip
import pandas as pd

# Some "global variable"
UCI_ARCHIVE = 'http://archive.ics.uci.edu/ml/machine-learning-databases/'

SAVE_DIR = '../data/'
DATA_EXT = '.csv'
DESC_EXT = '.txt'

# Create directory SAVE_DIR if it does not exist
if not os.path.exists(SAVE_DIR):
    os.makedirs(SAVE_DIR)

# List of datasets:
dataset = []

# name, relative URL, data file, description file (if empty, use the name), columns to eliminate, class label column (-1 is last)
dataset.append( ['glass', '', '', '', [0], -1] )
dataset.append( ['breast-cancer-wisconsin', '', '', '', [0], -1] )
dataset.append( ['pima-indians-diabetes', '', '', '', [], -1] )
dataset.append( ['sonar', 'undocumented/connectionist-bench/sonar/', 'sonar.all-data', '', [], -1] )
dataset.append( ['ionosphere', '', '', '', [0], -1] )
dataset.append( ['soybean-large', 'soybean/', '', '', [], 0] )
dataset.append( ['german-credit', 'statlog/german/', 'german.data-numeric', 'german.doc', [], -1] )
dataset.append( ['ecoli', '', '', '', [0], -1] )
dataset.append( ['voting-records', '', 'house-votes-84.data', 'house-votes-84.names', [], 0] )
dataset.append( ['liver-disorders', '', 'bupa.data', 'bupa.names', [], -1] )
dataset.append( ['letter-recognition', '', '', '', [], 0] )
dataset.append( ['forest-covertype', 'covtype/', 'covtype.data.gz', 'covtype.info', [], -1] )
dataset.append( ['iris', '', '', '', [], -1])


# Download from UCI repository

# Fill out missing entries in dataset
for dat in dataset:
    if dat[1] is '':
        dat[1] = dat[0] + '/'
    if dat[2] is '':
        dat[2] = dat[0] + '.data'
    if dat[3] is '':
        dat[3] = dat[0] + '.names'

# Download
print '\nUCI repository\n\n' + 'This will download and format ' + str(len(dataset)) + ' datasets.'
print 'They will be saved in the directory ' + SAVE_DIR + '\n'

for i_dat, dat in enumerate(dataset):
    if os.path.exists(SAVE_DIR + dat[0] + DATA_EXT):
        print ' ' + str(i_dat+1) + '. Skipping ' + dat[0] + ' (already exists)'
        continue
    print ' ' + str(i_dat+1) + '. Downloading ' + dat[0]

    # download datafile
    if dat[2][-3:] == '.gz': # The file is zipped
        urllib.urlretrieve(
            UCI_ARCHIVE + dat[1] + dat[2], SAVE_DIR + dat[2])
        gz_file = gzip.open(SAVE_DIR + dat[2])
        txt_file = open(SAVE_DIR + dat[0] + DATA_EXT, 'w')
        try:
            txt_file.write(gz_file.read())
        finally:
            txt_file.close()
        gz_file.close()
        os.remove(SAVE_DIR + dat[2])
    else:
        urllib.urlretrieve(
            UCI_ARCHIVE + dat[1] + dat[2], SAVE_DIR + dat[0] + DATA_EXT)

    # download description file
    urllib.urlretrieve(
        UCI_ARCHIVE + dat[1] + dat[3], SAVE_DIR + dat[0] + DESC_EXT)

    # Format by putting class label at the end and deleting non-informative
    # columns
    data_file = pd.read_csv(
        SAVE_DIR + dat[0] + DATA_EXT, header=None, na_values=['?'], sep=',')

    # If separator is not comma, try with space
    if data_file.shape[1] == 1:
        data_file = pd.read_csv(
            SAVE_DIR + dat[0] + DATA_EXT, header=None, na_values=['?'], sep='\s+')

    # Eliminate row if all entries are empty (this is the case for the last row of iris):
    for row in data_file.iterrows():
        if all(row[1].isnull()):
            data_file = data_file.drop(row[0], axis=0)

    # Get output label column
    if dat[5] == -1:
        dat[5] = data_file.shape[1] - 1
    data_file['out'] = pd.Categorical.from_array(data_file[dat[5]])

    # Drop useless columns
    data_file = data_file.drop([dat[5]], axis=1)
    data_file = data_file.drop(dat[4], axis=1)

    # Convert object types to numerical values
    iobj = data_file.dtypes == 'object'
    for col in iobj[iobj].keys():
        values = data_file[col].unique().tolist()
        if numpy.nan in values:     # do not replace empty entries
            values.remove(numpy.nan)
        for i, val in enumerate(values):
            # replace object with number
            data_file[col] = data_file[col].replace(val, i)

    # Rewrite the file
    data_file.to_csv(
        SAVE_DIR + dat[0] + DATA_EXT, header=None, index=False, sep=',')

print '\nAll done.\n'
